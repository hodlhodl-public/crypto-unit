Gem::Specification.new do |s|
  s.name = "crypto-unit"
  s.version = File.read(__dir__ + "/VERSION")
  s.authors = ["romanitup"]
  s.email = "romanitup@protonmail.com"
  s.summary = "Converts various BTC and LTC denominations"
  s.description = "Converts various BTC and LTC denominations"
  s.homepage = "https://gitlab.com/hodlhodl-public/crypi-unit"
  s.require_paths = ["lib"]
  s.extra_rdoc_files = [
    "LICENSE.txt",
    "README.md"
  ]
end
